const express = require("express");
const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

let array = []

app.post("/register", (req, res) => {

	if((req.body.username !== '') && (req.body.password !== '') && (req.body.fname !== '') && (req.body.lname !== '')){
		array.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please input all required data");
	}
});

app.post("/login", (req, res) => {
	
	let message;

	// Create a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < array.length;i++){

		// req.body == juan23 -- users[i].username == jane123
		if((req.body.username == array[i].username) && (req.body.password == array[i].password)){
			
			// Changes the message to be sent back by the response
			message = `User ${req.body.username} successfully login`

			break;
		}

		else{
			message = "Invalid username or password";
		}
	}
	res.send(message);



	
});




app.listen(port, () => console.log(`Server running at port ${port}`));