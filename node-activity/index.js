// ACTIVITY 1
//default method: GET
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response)=>response.json())
.then((json)=> console.log(`Title: ${json.title}
Body: ${json.body}`) )

// ACTIVTY 2

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Title',
		body: 'Updated Body'
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json))

